const http = require("http");
const port = 4000;

let directory = [
{
		"firstName": "Mary Jane",
		"lastName": "Dela Cruz",
		"mobileNo": "09123456789",
		"email": "mjdelacruz@mail.com",
		"password": 123
	},
	{
		"firstName": "John",
		"lastName": "Doe",
		"mobileNo": "09123456789",
		"email": "jdoe@mail.com",
		"password": 123
	}
];

const server = http.createServer((req, res) => {
	if(req.url === "/profile" && req.method === "GET"){
		res.writeHead(200, {"Content-Type": "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
	}
	if(req.url === "/profile" && req.method === "POST"){
		let requestBody = "";
		req.on('data', (data) => requestBody += data);
		req.on('end', (end) => {
			requestBody = JSON.parse(requestBody);
			let newUser = {
				"firstName": requestBody.firstName,
				"lastName": requestBody.lastName,
				"mobileNo": requestBody.mobileNo,
				"email": requestBody.email,
				"password": requestBody.password
			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type": "application/json"});
			res.write(JSON.stringify(directory));
			res.end();
		});
	}
});
server.listen(port);
console.log(`Server running at port ${port}`);